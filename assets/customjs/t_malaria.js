jQuery().ready(function (){ 
	jQuery("#listmalaria").jqGrid({ 
		url:'t_malaria/t_malariaxml', 
		emptyrecords: 'Nothing to display',
		datatype: "xml", 
		colNames:['id','NIK','NAMA PENDERITA','Puskesmas','Bulan','Tahun','Action'],
		rownumbers:true,
		width: 1021,
		height: 'auto',
		mtype: 'POST',
		altRows     : true,		
		colModel:[ 
				{name:'id',index:'id', width:25,align:'center',hidden:true}, 
				{name:'nik',index:'nik', width:75}, 
				{name:'nama',index:'nama', width:75,align:'center'}, 
				{name:'puskes',index:'puskes', width:75,align:'center'}, 
				{name:'bulan',index:'bulan', width:35,align:'center'}, 
				{name:'tahun',index:'tahun', width:35,align:'center'},
				{name:'myid',index:'myid', width:91,align:'center',formatter:formatterAction}
			],
			rowNum:10, 
			rowList:[10,20,30], 
			pager: jQuery('#pagermalaria'), 
			viewrecords: true, 
			sortorder: "desc",
			beforeRequest:function(){
				tahun=$('select[name=crtahunkematianibu] option:selected').text();
				carinama=$('#carinamamalaria').val()?$('#carinamamalaria').val():'';
				$('#listmalaria').setGridParam({postData:{'tahun':tahun,'carinama':carinama}})
			}
	}).navGrid('#pagermalaria',{edit:false,add:false,del:false,search:false});
	
	function formatterAction(cellvalue, options, rowObject) {
		var content = '';
		content  += '<a rel="' + cellvalue + '" class="icon-detail"  title="View"></a> ';
		content  += '<a rel="' + cellvalue + '" class="icon-edit" title="Edit?"></a>';
		content  += '<a rel="' + cellvalue + '" class="icon-delete" title="Hapus?"></a>';
		return content;
	}
	
	$("#listmalaria .icon-detail").live('click', function(f){
		if($(f.target).data('oneclicked')!='yes')
		{
			$("#t814","#tabs").empty();
			$("#t814","#tabs").load('t_malaria/detail'+'?id='+this.rel);
		}
		$(f.target).data('oneclicked','yes');
	});
	
	$("#listmalaria .icon-edit").live('click', function(f){
		if($(f.target).data('oneclicked')!='yes')
		{
			$("#t814","#tabs").empty();
			$("#t814","#tabs").load('t_malaria/edit'+'?id='+this.rel);
		}
		$(f.target).data('oneclicked','yes');		
	});
	
	function deldata(myid){
		achtungShowLoader();
		$.ajax({
			  url: 't_malaria/delete',
			  type: "post",
			  data: {id:myid},
			  dataType: "json",
			  success: function(msg){
				if(msg == 'OK'){
					$("#dialogmalaria").dialog("close");
					$.achtung({message: 'Proses Hapus Data Berhasil', timeout:5});
					$('#listmalaria').trigger("reloadGrid");							
				}
				else{						
					$("#dialogmalaria").dialog("close");
					$.achtung({message: 'Maaf Proses Hapus Data Gagal', timeout:5});
				}						
			  }
		  });
		achtungHideLoader();
	}
	
	$("#listmalaria .icon-delete").live('click', function(){
		var myid= this.rel;
		$("#dialogmalaria").dialog({
		  autoOpen: false,
          modal:true,
		  buttons : {
			"Confirm" : function() {
				deldata(myid);
			},
			"Cancel" : function() {
			  $(this).dialog("close");
			}
		  }
		});
		
		$("#dialogmalaria").dialog("open");
	});
	
	$('form').resize(function(e) {
		if($("#listmalaria").getGridParam("records")>0){
		jQuery('#listmalaria').setGridWidth(($(this).width()-28));
		}
	});
	
	function formattermou(cellvalue, options, rowObject) {
		var content = '';
		if(cellvalue){		
			content  += '<a href="tmp/t_malaria/' + cellvalue + '" style="color:blue;cursor:pointer" title="">download</a>';
		}else{
			content  += ' - ';
		}
		return content;
	}
	
	$('#malariaadd').click(function(){
		$("#t814","#tabs").empty();
		$("#t814","#tabs").load('t_malaria/add'+'?_=' + (new Date()).getTime());
	});
	
	
	jQuery.fn.reset = function (){
	  $(this).each (function() { this.reset(); });
	}
	$("#resetmalaria").live('click', function(event){
		event.preventDefault();
		$('#formmalaria').reset();
		$('#listmalaria').trigger("reloadGrid");
	});
	$("#carimalaria").live('click', function(event){
		event.preventDefault();
		$('#listmalaria').trigger("reloadGrid");
	});
})