<script>
$(document).ready(function(){
		jQuery("#listpasienberkunjung").jqGrid({ 
		url:'t_malaria/pasienberkunjung_xml', 
		emptyrecords: 'Nothing to display',
		datatype: "xml", 
		colNames:['ID','Nama Desa', 'Nama Kecamatan','Kabupaten/Kota','Tanggal Berkunjung'],
		rownumbers:true,
		width: 1049,
		height: 'auto',
		mtype: 'POST',
		altRows     : true,		
		colModel :[ 
			{name:'irow',index:'irow', width:10,align:'center',hidden:true},
			{name:'master_kelurahan_id',index:'master_kelurahan_id', width:100,align:'center'}, 
			{name:'master_kecamatan_id',index:'master_kecamatan_id', width:100}, 
			{name:'master_kabupaten_id',index:'master_kabupaten_id', width:100}, 
			{name:'tgl_kunjung',index:'tgl_kunjung', width:100}
			],
			rowNum:5, 
			rowList:[5,10,20,30], 
			pager: jQuery('#pagert_pasienberkunjung'),
			viewrecords: true, 
			sortorder: "desc",
			beforeRequest:function(){
				id1='<?=$data->REG_MALARIA?>';
				var idpuskesmas='<?=$data->KD_PUSKESMAS?>';
				$('#listpasienberkunjung').setGridParam({postData:{'myid1':id1,'idpuskesmas':idpuskesmas}})
			}	
	}).navGrid('#pagert_pasienberkunjung',{search:false});
	
});
</script>
<script>
	$('#backlistmalaria').click(function(){
		$("#t814","#tabs").empty();
		$("#t814","#tabs").load('t_malaria'+'?_=' + (new Date()).getTime());
	})
</script>
<div class="mycontent">
<div class="formtitle">Detail Kartu Penderita Malaria</div>
<div class="backbutton"><span class="kembali" id="backlistmalaria">kembali ke list</span></div>
</br>

<span id='errormsg'></span>
<form name="frApps" method="post" enctype="multipart/form-data">
	<fieldset>
		<span>
		<label>Bulan</label>
		<select name="BULAN" disabled="disabled" >
			<option value="">- pilih bulan -</option>
			<option value="01" <?=$data->BULAN=='01'?'selected':'';?> >Januari</option>
			<option value="02" <?=$data->BULAN=='02'?'selected':'';?> >Februari</option>
			<option value="03" <?=$data->BULAN=='03'?'selected':'';?> >Maret</option>
			<option value="04" <?=$data->BULAN=='04'?'selected':'';?> >April</option>
			<option value="05" <?=$data->BULAN=='05'?'selected':'';?> >Mei</option>
			<option value="06" <?=$data->BULAN=='06'?'selected':'';?> >Juni</option>
			<option value="07" <?=$data->BULAN=='07'?'selected':'';?> >Juli</option>
			<option value="08" <?=$data->BULAN=='08'?'selected':'';?> >Agustus</option>
			<option value="09" <?=$data->BULAN=='09'?'selected':'';?> >September</option>
			<option value="10" <?=$data->BULAN=='10'?'selected':'';?> >Oktober</option>
			<option value="11" <?=$data->BULAN=='11'?'selected':'';?> >November</option>
			<option value="12" <?=$data->BULAN=='12'?'selected':'';?> >Desember</option>
		</select>
		</span>
	</fieldset>
	<fieldset>
		<span>
		<label>Tahun*</label>
		<input type="text" name="TAHUN" id="tahun_ds_kematian_ibu" value="<?=$data->TAHUN?>"  disabled="disabled" />
		</span>
	</fieldset>
	</br>
	<fieldset>
		<span>
		<label>NIK*</label>
		<input type="text" name="nik"value="<?=$data->NO_PENGENAL?>"  disabled="disabled"  />
		</span>
	</fieldset>
	<fieldset>
		<span>
		<label>Nama Penderita</label>
		<input type="text" name="nama_penderita" value="<?=$data->NAMA_PENDERITA?>" disabled="disabled"/>
		</span>
	</fieldset>
	<br/>
	<div class="formtitle">Kartu Penderita Malaria</div>
	<fieldset class="fieldsetTop">
			<span>
				<label class="declabel2"><b>Pemeriksa: </b></label>
				<?php
					$pemeriksa=$data->PEMERIKSA;
				?>
			</span>
			<span>
				<label class="declabel2"><input type="checkbox" disabled="disabled" name="pemeriksa[]" value="Dokter" <? if (strpos($pemeriksa,'Dokter') !== false) { echo "Checked";}?> > Dokter</label>
			</span>
			<span>
				<label class="declabel2"><input type="checkbox" disabled="disabled" name="pemeriksa[]" value="Bidan" <? if (strpos($pemeriksa,'Bidan') !== false) { echo "Checked"; }?> > Bidan</label>
			</span>
			<span>
				<label class="declabel2"><input type="checkbox" disabled="disabled" name="pemeriksa[]" value="Perawat" <? if (strpos($pemeriksa,'Perawat') !== false) { echo "Checked";}?> > Perawat</label>
			</span>
	</fieldset><br/>
	<fieldset class="fieldsetTop">
		<span>
			<label class="declabel2" style="width:100%;"><b>Gejala Disebutkan semua gejala (bisa lebih dr satu) : </b></label>
		</span>
	</fieldset>
	<?php
			$gejala=$data->GEJALA;
	?>
	<fieldset class="fieldsetTop">
		<span>
			<label class="declabel2"><input type="checkbox" disabled="disabled" name="gejala[]" value="Demam" <? if (strpos($gejala,'Demam') !== false) { echo "Checked"; }?> > Demam</label>
			<label class="declabel2"><input type="checkbox" disabled="disabled" name="gejala[]" value="Mengigil" <? if (strpos($gejala,'Mengigil') !== false) { echo "Checked";}?> > Mengigil</label>
			<label class="declabel2"><input type="checkbox" disabled="disabled" name="gejala[]" value="Tidak Nafsu Makan" <? if (strpos($gejala,'Tidak Nafsu Makan') !== false) { echo "Checked";}?> > Tidak Nafsu Makan</label>
		</span>
		<span>			
			<label class="declabel2"><input type="checkbox" disabled="disabled" name="gejala[]" value="Berkeringat" <? if (strpos($gejala,'Berkeringat') !== false) { echo "Checked";}?> > Berkeringat</label>
			<label class="declabel2"><input type="checkbox" disabled="disabled" name="gejala[]" value="Diare" <? if (strpos($gejala,'Diare') !== false) { echo "Checked"; }?> > Diare</label>
		</span>
		<span>
			<label class="declabel2"><input type="checkbox" disabled="disabled" name="gejala[]" value="Sakit Kepala" <? if (strpos($gejala,'Sakit Kepala') !== false) { echo "Checked"; }?> > Sakit Kepala</label>
			<label class="declabel2"><input type="checkbox" disabled="disabled" name="gejala[]" value="Nyeri Sendi" <? if (strpos($gejala,'Nyeri Sendi') !== false) { echo "Checked"; }?> > Nyeri Sendi</label>
		</span>
		<span>
			<label class="declabel2"><input type="checkbox" disabled="disabled" name="gejala[]" value="Mual" <? if (strpos($gejala,'Mual') !== false) { echo "Checked"; }?> > Mual</label>
			<label class="declabel2"><input type="checkbox" disabled="disabled" name="gejala[]" value="Muntah" <? if (strpos($gejala,'Diare') !== false) { echo "Checked"; }?> > Muntah</label>
		</span>
	</fieldset>
	
	<br/>
	<fieldset>
		<span>
			<label style="width:100%;">
				<b>Tanggal Mulai Sakit (Kapan Gejala Mulai Terasa/Klinis) (Diisi Tahap III-IV) :</b>
			</label>
			<input type="text" disabled="disabled" value="<?=date('d/m/Y', strtotime($data->TGL_GEJALA))?>" disabled="disabled" name="tgl_gejala" id="tgl_gejala" class="input-datepicker mydate" />
		</span>
	</fieldset>
	<br/>
	<fieldset>
		<span>
			<label style="width:100%;">
			<b>Riwayat bepergian dan bermalam di daerah endemis malaria dalam 1 bulan terakhir sebelum sakit (ya/tidak)*  (tahap III-IV)<br/>
			Bila ya, sebutkan nama wilayah dan tanggal berkunjung</b>
			</label>
		</span>
	</fieldset>
	<div class="paddinggrid">
	<table id="listpasienberkunjung"></table>
	<div id="pagert_pasienberkunjung"></div>
	</div>
	<input type="hidden" disabled="disabled" name="pasienberkunjung_final" id="pasienberkunjung_final" />
	<fieldset id="fieldsdiagnosarawatjalan">
		<span>
			<label class="declabel2">Nama Desa</label>
			<input type="text" disabled="disabled" name="text" value="" id="nama_desa" style="width:255px;" />
		</span>
		<br/>
		<span>
			<label class="declabel2">Nama Kecamatan</label>
			<input type="text" disabled="disabled" name="text" value="" id="nama_kec" style="width:255px;" />
		</span>
		<br/>
		<span>
			<label class="declabel2">Nama Kabupaten/Kota</label>
			<input type="text" disabled="disabled" name="text" value="" id="nama_kab" style="width:255px;" />
		</span>
		<br/>
		<span>
			<label class="declabel2">Tanggal Berkunjung</label>
			<input type="text" disabled="disabled" name="text" value="" id="tgl_kunjung" style="width:255px;" class="input-datepicker mydate"/>
		</span>
		<br/>
		<span>
			<input type="button" value="Tambah" id="tambahkunjungid" />
			<input type="button" id="hapuskunjungid" value="Hapus" />
		</span>
	</fieldset>
	<br/>
	<table width="100%"><tr><td width="50%" valign="top">
	<fieldset>
		<span>
			<label style="width:100%;">
				<b>Riwayat pernah menderita penyakit malaria sebelumnya (ya/tidak)</b>
			</label>
		</span>
	</fieldset>
	<fieldset>
		<span>
			<label>
				Bila ya, sebutkan waktunya (Tgl-Bulan-Tahun) :
			</label>
			<input type="text" disabled="disabled" value="<?=date('d/m/Y', strtotime($data->TGL_BERKUNJUNG))?>" name="tgl_masa" id="tgl_masa" />
		</span>
	</fieldset>
	<fieldset>
		<span>
			<label>
				Obat malaria yang pernah diterima :
			</label>
			<input type="text" value="<?=$data->HISTORY_OBAT?>" disabled="disabled" name="history_obat" id="obatsearchmalaria"/>
		</span>
	</fieldset>
	<br/>
	<fieldset class="fieldsetTop">
		<span>
			<label>
				<b>Pemeriksaan Lab</b>
			</label>
		</span>
	</fieldset>
	<fieldset>
		<span>
			<label>
				a.	Tanggal Pemeriksaan	:
			</label>
			<input type="text" value="<?=date('d/m/Y', strtotime($data->TGL_PERIKSA))?>" disabled="disabled" name="tgl_periksa" id="tgl_periksa" class="input-datepicker mydate" />
		</span>
	</fieldset>
	<?php
			$metode_pemeriksaan=$data->METODE_PEMERIKSAAN;
	?>
	<fieldset>
		<span>
			<label>
				b. Metode Pemeriksaan :
			</label>
			<span>
				<label class="declabel2"><input type="checkbox" disabled="disabled" name="metode_pemeriksaan[]" value="KDT" <? if (strpos($metode_pemeriksaan,'KDT') !== false) { echo "Checked"; }?>> KDT</label>
				<label class="declabel2"><input type="checkbox" disabled="disabled" name="metode_pemeriksaan[]" value="Mikrosk" <? if (strpos($metode_pemeriksaan,'Mikrosk') !== false) { echo "Checked"; }?>> Mikrosk</label>
				<label class="declabel2"><input type="checkbox" disabled="disabled" name="metode_pemeriksaan[]" value="PCR" <? if (strpos($metode_pemeriksaan,'PCR') !== false) { echo "Checked"; }?>> PCR</label>
			</span>
		</span>
	</fieldset>
	<br/>
	<?php
			$jenis_parasit=json_decode($data->JENIS_PARASIT);
	?>
	<fieldset>
		<span>
			<label>
				<b>Jenis Parasit :</b>
			</label>
		</span>
	</fieldset>
	<fieldset>
		<span>
			<label>Pf</label>
			<input type="text" disabled="disabled" name="jenis_parasit[]" value="<?=$jenis_parasit[0]?>"/>
		</span>
	</fieldset>
	<fieldset>
		<span>
			<label>Pv</label>
			<input type="text" disabled="disabled" name="jenis_parasit[]" value="<?=$jenis_parasit[1]?>"/>
		</span>
	</fieldset>
	<fieldset>
		<span>
			<label>
				Pm
			</label>
			<input type="text" disabled="disabled" name="jenis_parasit[]" value="<?=$jenis_parasit[2]?>"/>
		</span>
	</fieldset>
	<fieldset>
		<span>
			<label>
				Po
			</label>
			<input type="text" disabled="disabled" name="jenis_parasit[]" value="<?=$jenis_parasit[3]?>"/>
		</span>
	</fieldset>
	<fieldset>
		<span>
			<label>
				Lainnya
			</label>
			<input type="text" disabled="disabled" name="jenis_parasit[]" value="<?=$jenis_parasit[4]?>"/>
		</span>
	</fieldset>
	<fieldset>
		<span>
			<label>
				Mix, sebutkan.....
			</label>
			<input type="text" disabled="disabled" name="jenis_parasit[]" value="<?=$jenis_parasit[5]?>"/>
		</span>
	</fieldset>
	<br/>
	<?php
			$pengobatan=$data->PENGOBATAN;
	?>
	<fieldset class="fieldsetTop">
		<span>
			<label>
				<b>Pengobatan :</b><br/>
				<i>(ACT : DHP, AAQ, )</i>
			</label>
			<span>
				<label class="declabel2"><input type="checkbox" disabled="disabled" name="pengobatan[]" value="ACT" <? if (strpos($pengobatan,'ACT') !== false) { echo "Checked"; }?>> ACT</label>
				<label class="declabel2"><input type="checkbox" disabled="disabled" name="pengobatan[]" <? if (strpos($pengobatan,'PRIMAQUINE') !== false) { echo "Checked"; }?> value="PRIMAQUINE"> PRIMAQUINE</label>
				<label class="declabel2"><input type="checkbox" disabled="disabled" name="pengobatan[]" <? if (strpos($pengobatan,'KINA') !== false) { echo "Checked"; }?> value="KINA"> KINA</label>
				<label class="declabel2">
					<input type="checkbox" disabled="disabled" name="pengobatan[]" value="Lainnya" id="lainnya"> Lainnya (sebutkan)...
					<input type="text" disabled="disabled" name="text" value="" onchange="$('#lainnya').val(this.value)"/>
				</label>
				
			</span>
		</span>
	</fieldset>
	<br/>
	<?php
			$keadaan_malaria=$data->KEADAAN_MALARIA;
	?>
	<fieldset class="fieldsetTop">
		<span>
			<label>
				<b>Keadaan Malaria :</b><br/>
			</label>
			<span>
				<label class="declabel2"><input type="radio" disabled="disabled" name="keadaan_malaria[]" <? if (strpos($keadaan_malaria,'Malaria tanpa komplikasi') !== false) { echo "Checked"; }?> value="Malaria tanpa komplikasi"> Malaria tanpa komplikasi</label>
				<label class="declabel2"><input type="radio" disabled="disabled" name="keadaan_malaria[]" <? if (strpos($keadaan_malaria,'Malaria dengan komplikasi') !== false) { echo "Checked"; }?> value="Malaria dengan komplikasi"> Malaria dengan komplikasi</label>
			</span>
		</span>
	</fieldset>
	<br/>
	<?php
			$follow_up=$data->FOLLOW_UP_H4;
	?>
	<fieldset class="fieldsetTop">
		<span>
			<label>
				<b>Follow Up Pengobatan :</b><br/>
			</label>
			<span>
			<div class="spanfloat">
				<label class="declabel2 min">Hari 4</label>
				<label class="declabel2 min"><input type="radio" disabled="disabled" name="follow_up[4][]" <? if (strpos($follow_up,'"4":["Pos"]') !== false) { echo "Checked"; }?> value="Pos"> Pos</label>
				<label class="declabel2 min"><input type="radio" disabled="disabled" name="follow_up[4][]" <? if (strpos($follow_up,'"4":["Neg"]') !== false) { echo "Checked"; }?> value="Neg"> Neg</label>
			</div>
			<div class="spanfloat">
				<label class="declabel2 min">Hari 14</label>
				<label class="declabel2 min"><input type="radio" disabled="disabled" name="follow_up[14][]" <? if (strpos($follow_up,'"14":["Pos"]') !== false) { echo "Checked"; }?> value="Pos"> Pos</label>
				<label class="declabel2 min"><input type="radio" disabled="disabled" name="follow_up[14][]" <? if (strpos($follow_up,'"14":["Pos"]') !== false) { echo "Checked"; }?> value="Neg"> Neg</label>
			</div>
			<div class="spanfloat">
				<label class="declabel2 min">Hari 28</label>
				<label class="declabel2 min"><input type="radio" disabled="disabled" name="follow_up[28][]" <? if (strpos($follow_up,'"28":["Pos"]') !== false) { echo "Checked"; }?> value="Pos"> Pos</label>
				<label class="declabel2 min"><input type="radio" disabled="disabled" name="follow_up[28][]" <? if (strpos($follow_up,'"28":["Pos"]') !== false) { echo "Checked"; }?> value="Neg"> Neg</label>
			</div>
			<div class="spanfloat">
				<label class="declabel2 min">3 Bulan</label>
				<label class="declabel2 min"><input type="radio" disabled="disabled" name="follow_up[90][]" <? if (strpos($follow_up,'"90":["Pos"]') !== false) { echo "Checked"; }?> value="Pos"> Pos</label>
				<label class="declabel2 min"><input type="radio" disabled="disabled" name="follow_up[90][]" <? if (strpos($follow_up,'"90":["Pos"]') !== false) { echo "Checked"; }?> value="Neg"> Neg</label>
			</div>
			</span>
		</span>
	</fieldset>
	<br/>
	</td><td width="50%" valign="top">
	<?php
			$efek_samping=$data->EFEK_SAMPING;
	?>
	<fieldset class="fieldsetTop">
		<span>
			<label>
				<b>Efek Samping Pengobatan :</b><br/>
			</label>
			<span>
				<label class="declabel2"><input type="checkbox" disabled="disabled" name="efek_samping[]" <? if (strpos($efek_samping,'Mual') !== false) { echo "Checked"; }?> value="Mual"> Mual</label>
				<label class="declabel2"><input type="checkbox" disabled="disabled" name="efek_samping[]" <? if (strpos($efek_samping,'Lemas') !== false) { echo "Checked"; }?> value="Lemas"> Lemas</label>
				<label class="declabel2"><input type="checkbox" disabled="disabled" name="efek_samping[]" <? if (strpos($efek_samping,'Pusing') !== false) { echo "Checked"; }?> value="Pusing"> Pusing</label>
				<label class="declabel2"><input type="checkbox" disabled="disabled" name="efek_samping[]" <? if (strpos($efek_samping,'Muntah') !== false) { echo "Checked"; }?> value="Muntah"> Muntah</label>
				<label class="declabel2"><input type="checkbox" disabled="disabled" name="efek_samping[]" <? if (strpos($efek_samping,'Pingsang') !== false) { echo "Checked"; }?> value="Pingsang"> Pingsang</label>
				<label class="declabel2"><input type="checkbox" disabled="disabled" name="efek_samping[]" <? if (strpos($efek_samping,'Kejang') !== false) { echo "Checked"; }?> value="Kejang"> Kejang</label>
				<label class="declabel2"><input type="checkbox" disabled="disabled" name="efek_samping[]" <? if (strpos($efek_samping,'Sakit Kepala') !== false) { echo "Checked"; }?> value="Sakit Kepala"> Sakit Kepala</label>
			</span>
		</span>
	</fieldset>
	<br/>
	<fieldset class="fieldsetTop">
		<span>
			<label>
				<b>Rujukan Penderita :</b>
			</label>
		</span>
	</fieldset>
	<?php
			$dirujuk_dari=$data->DIRUJUK_DARI;
	?>
	<fieldset class="fieldsetTop">
		<span>
			<label>
				A. Dirujuk Dari : 
			</label>
			<span>
				<label class="declabel2"><input type="radio" disabled="disabled" name="dirujuk_dari[]" <? if (strpos($dirujuk_dari,'Pustu') !== false) { echo "Checked"; }?> value="Pustu"> Pustu</label>
				<label class="declabel2"><input type="radio" disabled="disabled" name="dirujuk_dari[]" <? if (strpos($dirujuk_dari,'Poskesdes') !== false) { echo "Checked"; }?> value="Poskesdes"> Poskesdes</label>
				<label class="declabel2"><input type="radio" disabled="disabled" name="dirujuk_dari[]" <? if (strpos($dirujuk_dari,'Polindes') !== false) { echo "Checked"; }?> value="Polindes/Bidan Desa"> Polindes/Bidan Desa</label>
				<label class="declabel2"><input type="radio" disabled="disabled" name="dirujuk_dari[]" <? if (strpos($dirujuk_dari,'Klinik') !== false) { echo "Checked"; }?> value="Klinik/Praktek Swasta"> Klinik/Praktek Swasta</label>
				<label class="declabel2"><input type="radio" disabled="disabled" name="dirujuk_dari[]" <? if (strpos($dirujuk_dari,'Kader') !== false) { echo "Checked"; }?> value="Kader/Posmaldes"> Kader/Posmaldes</label>
			</span>
		</span>
	</fieldset>
	<?php
			$dirujuk_ke=$data->DIRUJUK_KE;
	?>
	<fieldset class="fieldsetTop">
		<span>
			<label>
				B. Dirujuk Ke : 
			</label>
			<span>
				<label class="declabel2"><input type="radio" disabled="disabled" name="dirujuk_ke[]" <? if (strpos($dirujuk_ke,'Rumah Sakit') !== false) { echo "Checked"; }?> value="Rumah Sakit"> Rumah Sakit</label>
				<label class="declabel2"><input type="radio" disabled="disabled" name="dirujuk_ke[]" <? if (strpos($dirujuk_ke,'Puskesmas') !== false) { echo "Checked"; }?> value="Puskesmas  lain"> Puskesmas  lain</label>
			</span>
		</span>
	</fieldset>
	<br/>
	<?php
			$hasil_obat=$data->HASIL_OBAT;
	?>
	<fieldset class="fieldsetTop">
		<span>
			<label>
				<b>Hasil Akhir Pengobatan :</b> 
			</label>
			<span>
				<label class="declabel2"><input type="radio" disabled="disabled" name="hasil_obat[]" <? if (strpos($hasil_obat,'SEMBUH') !== false) { echo "Checked"; }?> value="SEMBUH"> SEMBUH</label>
				<label class="declabel2"><input type="radio" disabled="disabled" name="hasil_obat[]" <? if (strpos($hasil_obat,'MENINGGAL') !== false) { echo "Checked"; }?> value="MENINGGAL"> MENINGGAL</label>
				<label class="declabel2"><input type="radio" disabled="disabled" name="hasil_obat[]" <? if (strpos($hasil_obat,'Follow Up Tidak Lengkap') !== false) { echo "Checked"; }?> value="Follow Up Tidak Lengkap"> Follow Up Tidak Lengkap</label>
			</span>
			<label style="width:100%;">
				<i>hasil pengobatan: diisi pada saat follow up terakhir merujuk pertanyaan no.19 </i>
			</label>
		</span>
		<?php
			$gagal_obat=$data->GAGAL_OBAT;
		?>
		<span>
			<label>
				<b>Gagal Pengobatan :</b>
			</label>
			<span>
				<label class="declabel2"><input type="checkbox" disabled="disabled" name="gagal_obat[]" <? if (strpos($gagal_obat,'Faktor Obat') !== false) { echo "Checked"; }?> value="Faktor Obat"> Faktor Obat</label>
				<label class="declabel2"><input type="checkbox" disabled="disabled" name="gagal_obat[]" <? if (strpos($gagal_obat,'Kepatuhan') !== false) { echo "Checked"; }?> value="Kepatuhan"> Kepatuhan</label>
			</span>
		</span>
	</fieldset>
	<br/>
	<?php
			$asal_penularan=$data->ASAL_PENULARAN;
	?>
	<fieldset class="fieldsetTop">
		<span>
			<label>
				<b>KLASIFIKASI ASAL PENULARAN</b>
			</label>
			<span>
				<label class="declabel2"><input type="radio" disabled="disabled" name="asal_penularan[]" <? if (strpos($asal_penularan,'Indigenous') !== false) { echo "Checked"; }?> value="Indigenous"> Indigenous</label>
				<label class="declabel2"><input type="radio" disabled="disabled" name="asal_penularan[]" <? if (strpos($asal_penularan,'Import') !== false) { echo "Checked"; }?> value="Import"> Import</label>
				<label class="declabel2"><input type="radio" disabled="disabled" name="asal_penularan[]" <? if (strpos($asal_penularan,'Tidak Diketahui') !== false) { echo "Checked"; }?> value="Tidak Diketahui"> Tidak Diketahui</label>
			</span>
			<label style="width:100%;">
				<i>(diisi pada tahap eliminasi dan pemeliharaan)</i>
			</label>
		</span>
	</fieldset>
	<br/>
	<?php
			$asal_kegiatan_penemuan=$data->ASAL_KEGIATAN_PENEMUAN;
	?>
	<fieldset class="fieldsetTop">
		<span>
			<label>
				<b>ASAL KEGIATAN PENEMUAN PENDERITA :</b> 
			</label>
			<span>
				<label class="declabel2"><input type="radio" disabled="disabled" name="asal_kegiatan_penemuan[]" <? if (strpos($asal_kegiatan_penemuan,'PCD') !== false) { echo "Checked"; }?> value="PCD"> PCD</label>
				<label class="declabel2"><input type="radio" disabled="disabled" name="asal_kegiatan_penemuan[]" <? if (strpos($asal_kegiatan_penemuan,'ACD') !== false) { echo "Checked"; }?> value="ACD"> ACD</label>
				<label class="declabel2"><input type="radio" disabled="disabled" name="asal_kegiatan_penemuan[]" <? if (strpos($asal_kegiatan_penemuan,'MBS') !== false) { echo "Checked"; }?> value="MBS"> MBS</label>
				<label class="declabel2"><input type="radio" disabled="disabled" name="asal_kegiatan_penemuan[]" <? if (strpos($asal_kegiatan_penemuan,'MFS') !== false) { echo "Checked"; }?> value="MFS"> MFS</label>
				<label class="declabel2"><input type="radio" disabled="disabled" name="asal_kegiatan_penemuan[]" <? if (strpos($asal_kegiatan_penemuan,'Survei Kontak') !== false) { echo "Checked"; }?> value="Survei Kontak"> Survei Kontak</label>
				<label class="declabel2"><input type="radio" disabled="disabled" name="asal_kegiatan_penemuan[]" <? if (strpos($asal_kegiatan_penemuan,'Kader') !== false) { echo "Checked"; }?> value="Kader"> Kader</label>
				<label class="declabel2"><input type="radio" disabled="disabled" name="asal_kegiatan_penemuan[]" <? if (strpos($asal_kegiatan_penemuan,'Follow up') !== false) { echo "Checked"; }?> value="Follow up"> Follow up</label>
				<label class="declabel2"><input type="radio" disabled="disabled" name="asal_kegiatan_penemuan[]" <? if (strpos($asal_kegiatan_penemuan,'PE') !== false) { echo "Checked"; }?> value="PE"> PE</label>
			</span>
		</span>
	</fieldset>
	</td></tr></table>
	<br />
</form>
</div >