<script type="text/javascript" src="<?=base_url()?>assets/customjs/t_malaria.js"></script>

<div class="mycontent">
<div id="dialogmalaria" style="color:red;font-size:.75em;display:none" title="Confirmation Required">
  Hapus Data?
</div>
<div class="formtitle">Data Malaria</div>
	<form id="formmalaria">
		<div class="gridtitle">Daftar Penderita Malaria<span class="tambahdata" id="malariaadd">Input Kartu Malaria</span></div>
		
		<fieldset style="margin:0 13px 0 13px ">
			<span>
			<label>Tanggal Input (dd-mm-yyyy)</label>
			<input type="text" name="dari" class="dari" id="darimalaria"/>
			sampai
			<input type="text" name="sampai" class="sampai" id="sampaimalaria"/>
			</span>						
		</fieldset>
		<fieldset style="margin:0 13px 0 13px ">
			<span>
			<label>Cari Nama Penderita</label>
			<input type="text" name="carinama" id="carinamamalaria"/>
			<input type="submit" class="cari" value="&nbsp;Cari&nbsp;" id="carimalaria"/>
			<input type="submit" class="reset" value="&nbsp;Reset&nbsp;" id="resetmalaria"/>
			</span>
		</fieldset>
		
		<div class="paddinggrid">
		<table id="listmalaria"></table>
		<div id="pagermalaria"></div>
		</div >
	</form>
</div>