<script type="text/javascript" src="<?=base_url()?>assets/js/date.format.js"></script>
<script type="text/javascript" src="<?=base_url()?>assets/js/ajax-load-global.js"></script>
<script>
$(document).ready(function(){
		$('#form1malariaadd').ajaxForm({
			beforeSend: function() {
				achtungShowLoader();	
			},
			uploadProgress: function(event, position, total, percentComplete) {
			},
			complete: function(xhr) {
				achtungHideLoader();
				if(xhr.responseText!=='OK'){
					$.achtung({message: xhr.responseText, timeout:5});
				}else{
					$.achtung({message: 'Proses Tambah Data Berhasil', timeout:5});
					$("#t814","#tabs").empty();
					$("#t814","#tabs").load('t_malaria'+'?_=' + (new Date()).getTime());
				}
			}
		});
		
		jQuery("#listpasienberkunjung").jqGrid({
			datatype: 'clientSide',
			rownumbers:true,
			width: 1049,
			height: 'auto',
			colNames:['Nama Desa', 'Nama Kecamatan','Kabupaten/Kota','Tanggal Berkunjung'],
			colModel :[ 
			{name:'master_kelurahan_id',index:'master_kelurahan_id', width:100,align:'center'}, 
			{name:'master_kecamatan_id',index:'master_kecamatan_id', width:100}, 
			{name:'master_kabupaten_id',index:'master_kabupaten_id', width:100}, 
			{name:'tgl_kunjung',index:'tgl_kunjung', width:100}
			],
			rowNum:35,
			viewrecords: true
		}); 
		
		var berkunjungid = 0;
		$('#tambahkunjungid').click(function(){
			var myfirstrow = {master_kelurahan_id:$('#master_kelurahan_id').val(), master_kecamatan_id:$('#master_kecamatan_id').val(), master_kabupaten_id:$('#master_kabupaten_id').val(), tgl_kunjung:$('#tgl_kunjung').val()};
			jQuery("#listpasienberkunjung").addRowData(berkunjungid+1, myfirstrow);
			$('#master_kelurahan_id').val('');
			$('#master_kecamatan_id').val('');
			$('#master_kabupaten_id').val('');
			$('#tgl_kunjung').val('');
			if(confirm('Tambah Data Kunjungan Lain?')){
				$('#master_propinsi_id').val('');
				$('#master_propinsi_id_hidden').val('');
				$('#master_kabupaten_id_hidden').val('');
				$('#master_kecamatan_id_hidden').val('');
				$('#master_kelurahan_id_hidden').val('');
			}else{
				$('#master_propinsi_id').val('');
				$('#master_propinsi_id_hidden').val('');
				$('#master_kabupaten_id_hidden').val('');
				$('#master_kecamatan_id_hidden').val('');
				$('#master_kelurahan_id_hidden').val('');
				$('#tgl_masa').focus();
			}
		})
		
		$('#hapuskunjungid').click(function(){
			jQuery("#listpasienberkunjung").clearGridData();
		})
		
		
		$("#form1malariaadd").validate({
			rules: {
				tanggal_inspeksi: {
					date:true,
					required: true
				}
			},
			messages: {
				tanggal_inspeksi: {
					required:"Silahkan Lengkapi Data"
				}
			}
		});
		
		$('#master_propinsi_id_hidden').focus(function(){
			$("#dialogcari_master_propinsi_id").dialog({
				autoOpen: false,
				modal:true,
				width: 545,
				height: 455,
				buttons : {
					"Cancel" : function() {
					  $(this).dialog("close");
					}
				}
			});
			$('#dialogcari_master_propinsi_id').load('c_master_propinsi/masterpropinsipopup?id_caller=form1malariaadd', function() {
				$("#dialogcari_master_propinsi_id").dialog("open");
			});
		});
		
		$('#master_kabupaten_id_hidden').focus(function(){
			$("#dialogcari_master_kabupaten_id").dialog({
				autoOpen: false,
				modal:true,
				width: 545,
				height: 455,
				buttons : {
					"Cancel" : function() {
					  $(this).dialog("close");
					}
				}
			});
			$('#dialogcari_master_kabupaten_id').load('c_master_kabupaten/masterkabupatenpopup?id_caller=form1malariaadd', function() {
				$("#dialogcari_master_kabupaten_id").dialog("open");
			});
		});
		
		$('#master_kecamatan_id_hidden').focus(function(){
			$("#dialogcari_master_kecamatan_id").dialog({
				autoOpen: false,
				modal:true,
				width: 545,
				height: 455,
				buttons : {
					"Cancel" : function() {
					  $(this).dialog("close");
					}
				}
			});
			$('#dialogcari_master_kecamatan_id').load('c_master_kecamatan/masterkecamatanpopup?id_caller=form1malariaadd', function() {
				$("#dialogcari_master_kecamatan_id").dialog("open");
			});
		});
		
		$('#master_kelurahan_id_hidden').focus(function(){
			$("#dialogcari_master_kelurahan_id").dialog({
				autoOpen: false,
				modal:true,
				width: 545,
				height: 455,
				buttons : {
					"Cancel" : function() {
					  $(this).dialog("close");
					}
				}
			});
			$('#dialogcari_master_kelurahan_id').load('c_master_kelurahan/masterkelurahanpopup?id_caller=form1malariaadd', function() {
				$("#dialogcari_master_kelurahan_id").dialog("open");
			});
		});
		
		$("#kabupaten_kotat_pendaftaranadd").remoteChained("#provinsit_pendaftaranadd", "<?=site_url('t_masters/getKabupatenByProvinceId')?>");
		$("#kecamatant_pendaftaranadd").remoteChained("#kabupaten_kotat_pendaftaranadd", "<?=site_url('t_masters/getKecamatanByKabupatenId')?>");
		$("#kelurahant_pendaftaranadd").remoteChained("#kecamatant_pendaftaranadd", "<?=site_url('t_masters/getKelurahanByKecamatanId')?>");
				
		
})
$("#tanggalmalaria").mask("99/99/9999");

$('#form1malariaadd :submit').click(function(e) {
	//e.preventDefault();
	if($("#form1malariaadd").valid()) {
		if(kumpularray())$('#form1malariaadd').submit();
	}
	return false;
});

</script>
<script>
function kumpularray(){
		if($('#listpasienberkunjung').getGridParam("records")>0){
			var rows= jQuery("#listpasienberkunjung").jqGrid('getRowData');
			var paras=new Array();
			for(var i=0;i<rows.length;i++){
				var row = rows[i];
				paras.push(JSON.stringify(row));
				//paras.push($.param(row));
			}
			$("#pasienberkunjung_final").val(JSON.stringify(paras));
		}
		
		return true;
	}
</script>
<script>
	$('#backlistmalaria').click(function(){
		$("#t814","#tabs").empty();
		$("#t814","#tabs").load('t_malaria'+'?_=' + (new Date()).getTime());
	});

	function get_dukcapil(){
			var NIK = $('#nobpjs_pendaftaranadd').val();
			$.post( "<?=site_url('t_pendaftaran/getdukcapil?nik=')?>"+NIK, {}).done(function( jsondata ) {
				try {
					obj = jQuery.parseJSON(jsondata);
				} catch (exception) {
					//alert("Data Ada yang salah : " + jsondata);
					get_dukcapil();
					return false;
				}
				
				var metaData = obj.GetPendudukResponse.metaData;
				if(metaData.code != "200"){
					alert("Data tidak ditemukan");
				}
				
				if(metaData.code == "200"){
					var response = obj.GetPendudukResponse.penduduk;
					
					var strtgl = response.TGL_LHR;
					var tgl_lahir = strtgl.substring(8, 10)+'/'+strtgl.substring(5, 7)+'/'+strtgl.substring(0, 4);
					$('#tanggal_lahirt_pendaftaranadd').val(tgl_lahir);
					
					$('#agamat_pendaftaranadd option:selected').text(response.AGAMA);
					$('#alamat_pendaftaranadd').val(response.ALAMAT);
					//$('#no_asuransi_pasienid').val(response.EKTP_STATUS);
					$('#gol_daraht_pendaftaranadd option:selected').text(response.GOL_DARAH);
					
					if(response.JENIS_KLMIN.trim() === "LAKI-LAKI"){
						$('#jenis_kelamint_pendaftaranadd').val('L');
					}else{
						$('#jenis_kelamint_pendaftaranadd').val('P');
					}
					
					$('#pekerjaant_pendaftaranadd option:selected').text(response.JENIS_PKRJN);
					//$('#kdprovider_pendaftaranadd').val(response.KAB_NAME);
					//$('#nmProvider_pendaftaranadd').val(response.KEC_NAME);
					//$('#kdjenispeserta_pendaftaranadd').val(response.KEL_NAME);
					$('#nama_lengkapt_pendaftaranadd').val(response.NAMA_LGKP);
					$('#nama_ayaht_pendaftaranadd').val(response.NAMA_LGKP_AYAH);
					$('#nama_ibut_pendaftaranadd').val(response.NAMA_LGKP_IBU);
					
					$("#provinsit_pendaftaranadd option:selected").text(response.PROP_NAME);
					$("#kabupaten_kotat_pendaftaranadd").remoteChained("#provinsit_pendaftaranadd", "<?=site_url('t_masters/getKabupatenByProvinceId')?>");
					
					setTimeout(function(){  
						$("#kabupaten_kotat_pendaftaranadd option:selected").text(response.KAB_NAME);
						$("#kecamatant_pendaftaranadd").remoteChained("#kabupaten_kotat_pendaftaranadd", "<?=site_url('t_masters/getKecamatanByKabupatenId')?>");
					}, 3000);
					
					setTimeout(function(){ 
						$("#kecamatant_pendaftaranadd option:selected").text(response.KEC_NAME);
						$("#kelurahant_pendaftaranadd").remoteChained("#kecamatant_pendaftaranadd", "<?=site_url('t_masters/getKelurahanByKecamatanId')?>");
					}, 6000);
					
					setTimeout(function(){ 
						$("#kelurahant_pendaftaranadd option:selected").text(response.KEL_NAME);
					}, 9000);
					
					$('#no_kkt_pendaftaranadd').val(response.NO_KK);
					//$('#nmjenispeserta_pendaftaranadd').val(response.NO_RT);
					//$('#nmjenispeserta_pendaftaranadd').val(response.NO_RW);
					$('#pendidikan_akhirt_pendaftaranadd option:selected').text(response.PDDK_AKH);
					//$('#nmjenispeserta_pendaftaranadd').val(response.PNYDNG_CCT);
					//$('#nmjenispeserta_pendaftaranadd').val(response.PROP_NAME);
					$('#status_nikaht_pendaftaranadd option:selected').text(response.STATUS_KAWIN);
					//$('#nmjenispeserta_pendaftaranadd').val(response.STAT_HBKEL);
					$('#tempat_lahirt_pendaftaranadd').val(response.TMPT_LHR);
				}
		});
	  }
	  
	  function get_bpjs_dukcapil(){
			get_dukcapil();
	  }	
</script>
<div id="dialog_cari_namapuskesmas" title="Master Puskesmas"></div>
<div id="dialogcari_master_propinsi_id" title="Master Provinsi"></div>
<div id="dialogcari_master_kabupaten_id" title="Master Kabupaten"></div>
<div id="dialogcari_master_kecamatan_id" title="Master Kecamatan"></div>
<div id="dialogcari_master_kelurahan_id" title="Master Kelurahan"></div>
<div class="mycontent">
<div class="formtitle">Tambah Data Malaria</div>
<div class="backbutton"><span class="kembali" id="backlistmalaria">kembali ke list</span></div>
</br>

<span id='errormsg'></span>
<form name="frApps" id="form1malariaadd" method="post" action="<?=site_url('t_malaria/addprocess')?>" enctype="multipart/form-data">	

	<fieldset>
		<span>
		<label>Propinsi</label>
		<input type="text"  name="" id="textid" value="<?=$this->session->userdata('nama_propinsi')?>" disabled />
		</span>
	</fieldset>
	<fieldset>
		<span>
		<label>Kabupaten</label>
		<input type="text"  name="" id="textid" value="<?=$this->session->userdata('nama_kabupaten')?>" disabled />
		</span>
	</fieldset>
	<fieldset>
		<span>
		<label>Kecamatan</label>
		<input type="text"  name="" id="textid" value="<?=$this->session->userdata('nama_kecamatan')?>" disabled />
		</span>
	</fieldset>
	<fieldset>
		<span>
		<label>Puskesmas</label>
		<input type="text"  name="" id="textid" value="<?=$this->session->userdata('puskesmas')?>" disabled />
		<input type="hidden"  name="kd_puskesmas" id="kd_puskesmas" value="<?=$this->session->userdata('kd_puskesmas')?>" />
		</span>
	</fieldset>
	<fieldset>
		<span>
		<label>Bulan*</label>
		<select name="bulan" required>
			<option value="">- pilih bulan -</option>
			<option value="01">Januari</option>
			<option value="02">Februari</option>
			<option value="03">Maret</option>
			<option value="04">April</option>
			<option value="05">Mei</option>
			<option value="06">Juni</option>
			<option value="07">Juli</option>
			<option value="08">Agustus</option>
			<option value="09">September</option>
			<option value="10">Oktober</option>
			<option value="11">November</option>
			<option value="12">Desember</option>
		</select>
		</span>
	</fieldset>
	<fieldset>
		<span>
		<label>Tahun*</label>
		<input type="text" name="tahun" id="tahun_kartu_malaria" value="" required />
		</span>
	</fieldset>
	</br>
	<fieldset>
		<span>
		<label>NIK*</label>
		<input type="text" name="nik" id="nobpjs_pendaftaranadd" style="width:130px;" value="" />
		<input type="button" value="Cari" onclick="get_bpjs_dukcapil()"/>
		</span>
	</fieldset>
	<fieldset>
		<span>
		<label>Nama Penderita</label>
		<input type="text" id="nama_lengkapt_pendaftaranadd" name="nama_penderita" value=""/>
		</span>
	</fieldset>
	<br/>
	<div class="formtitle">Kartu Penderita Malaria</div>
	<fieldset class="fieldsetTop">
			<span>
				<label class="declabel2"><b>Pemeriksa: </b></label>
			</span>
			<span>
				<label class="declabel2"><input type="checkbox" name="pemeriksa[]" value="Dokter"> Dokter</label>
			</span>
			<span>
				<label class="declabel2"><input type="checkbox" name="pemeriksa[]" value="Bidan"> Bidan</label>
			</span>
			<span>
				<label class="declabel2"><input type="checkbox" name="pemeriksa[]" value="Perawat"> Perawat</label>
			</span>
	</fieldset><br/>
	<fieldset class="fieldsetTop">
		<span>
			<label class="declabel2" style="width:100%;"><b>Gejala Disebutkan semua gejala (bisa lebih dr satu) : </b></label>
		</span>
	</fieldset>
	<fieldset class="fieldsetTop">
		<span>
			<label class="declabel2"><input type="checkbox" name="gejala[]" value="Demam"> Demam</label>
			<label class="declabel2"><input type="checkbox" name="gejala[]" value="Mengigil"> Mengigil</label>
			<label class="declabel2"><input type="checkbox" name="gejala[]" value="Tidak Nafsu Makan"> Tidak Nafsu Makan</label>
		</span>
		<span>			
			<label class="declabel2"><input type="checkbox" name="gejala[]" value="Berkeringat"> Berkeringat</label>
			<label class="declabel2"><input type="checkbox" name="gejala[]" value="Diare"> Diare</label>
		</span>
		<span>
			<label class="declabel2"><input type="checkbox" name="gejala[]" value="Sakit Kepala"> Sakit Kepala</label>
			<label class="declabel2"><input type="checkbox" name="gejala[]" value="Nyeri Sendi"> Nyeri Sendi</label>
		</span>
		<span>
			<label class="declabel2"><input type="checkbox" name="gejala[]" value="Mual"> Mual</label>
			<label class="declabel2"><input type="checkbox" name="gejala[]" value="Muntah"> Muntah</label>
		</span>
	</fieldset>
	<br/>
	<fieldset>
		<span>
			<label style="width:100%;">
				<b>Tanggal Mulai Sakit (Kapan Gejala Mulai Terasa/Klinis) (Diisi Tahap III-IV) :</b>
			</label>
			<input type="text" name="text" value="" name="tgl_gejala" id="tgl_gejala" class="input-datepicker mydate"/>
		</span>
	</fieldset>
	<br/>
	<fieldset>
		<span>
			<label style="width:100%;">
			<b>Riwayat bepergian dan bermalam di daerah endemis malaria dalam 1 bulan terakhir sebelum sakit (ya/tidak)*  (tahap III-IV)<br/>
			Bila ya, sebutkan nama wilayah dan tanggal berkunjung</b>
			</label>
		</span>
	</fieldset>
	<table id="listpasienberkunjung"></table>
	<input type="hidden" name="pasienberkunjung_final" id="pasienberkunjung_final" />
	<fieldset>
		<span>
		<label>Provinsi</label>
		<input type="text" name="kodeprovinsi" id="master_propinsi_id_hidden" value="" readonly  />
		<input type="text" placeholder="Provinsi" name="master_propinsi_id" id="master_propinsi_id" readonly value="" />
		</span>
	</fieldset>
	<fieldset>
		<span>
		<label>Kabupaten/Kota</label>
		<input type="text" name="kodekabupaten" id="master_kabupaten_id_hidden" value="" readonly  />
		<input type="text" placeholder="Kabupaten" name="master_kabupaten_id" id="master_kabupaten_id" readonly value="" />
		</span>
	</fieldset>
	<fieldset>
		<span>
		<label>Kecamatan</label>
		<input type="text" name="kodekecamatan" id="master_kecamatan_id_hidden" value="" readonly  />
		<input type="text" placeholder="Kecamatan" name="master_kecamatan_id" id="master_kecamatan_id" readonly value="" />
		</span>
	</fieldset>
	<fieldset>
		<span>
		<label>Kelurahan/Desa</label>
		<input type="text" name="kodekelurahan" id="master_kelurahan_id_hidden" value="" readonly  />
		<input type="text" placeholder="Kelurahan/Desa" name="lurah" id="master_kelurahan_id" readonly value="" />
		</span>
	</fieldset>
	<fieldset>
		<span>
			<label class="declabel2">Tanggal Berkunjung</label>
			<input type="text" name="text" value="" id="tgl_kunjung" class="input-datepicker mydate"/>
		</span>
	</fieldset>
	<fieldset>
		<br/>
		<span>
			<input type="button" value="Tambah" id="tambahkunjungid" />
			<input type="button" id="hapuskunjungid" value="Hapus" />
		</span>
	</fieldset>
	<br/>
	<table width="100%"><tr><td width="50%" valign="top">
	<fieldset>
		<span>
			<label style="width:100%;">
				<b>Riwayat pernah menderita penyakit malaria sebelumnya (ya/tidak)</b>
			</label>
		</span>
	</fieldset>
	<fieldset>
		<span>
			<label>
				Bila ya, sebutkan waktunya (Tgl-Bulan-Tahun) :
			</label>
			<input type="text" name="text" value="" name="tgl_masa" id="tgl_masa" class="input-datepicker mydate"/>
		</span>
	</fieldset>
	<fieldset>
		<span>
			<label>
				Obat malaria yang pernah diterima :
			</label>
			<input type="text" name="text" value="" name="history_obat" id="obatsearchmalaria"/>
		</span>
	</fieldset>
	<br/>
	<fieldset class="fieldsetTop">
		<span>
			<label>
				<b>Pemeriksaan Lab</b>
			</label>
		</span>
	</fieldset>
	<fieldset>
		<span>
			<label>
				a.	Tanggal Pemeriksaan	:
			</label>
			<input type="text" name="text" value="" name="tgl_periksa" id="tgl_periksa" class="input-datepicker mydate" />
		</span>
	</fieldset>
	<fieldset>
		<span>
			<label>
				b. Metode Pemeriksaan :
			</label>
			<span>
				<label class="declabel2"><input type="checkbox" name="metode_pemeriksaan[]" value="KDT"> KDT</label>
				<label class="declabel2"><input type="checkbox" name="metode_pemeriksaan[]" value="Mikrosk"> Mikrosk</label>
				<label class="declabel2"><input type="checkbox" name="metode_pemeriksaan[]" value="PCR"> PCR</label>
			</span>
		</span>
	</fieldset>
	<br/>
	<fieldset>
		<span>
			<label>
				<b>Jenis Parasit :</b>
			</label>
		</span>
	</fieldset>
	<fieldset>
		<span>
			<label>Pf</label>
			<input type="text" name="jenis_parasit[]" value=""/>
		</span>
	</fieldset>
	<fieldset>
		<span>
			<label>Pv</label>
			<input type="text" name="jenis_parasit[]" value=""/>
		</span>
	</fieldset>
	<fieldset>
		<span>
			<label>
				Pm
			</label>
			<input type="text" name="jenis_parasit[]" value=""/>
		</span>
	</fieldset>
	<fieldset>
		<span>
			<label>
				Po
			</label>
			<input type="text" name="jenis_parasit[]" value=""/>
		</span>
	</fieldset>
	<fieldset>
		<span>
			<label>
				Lainnya
			</label>
			<input type="text" name="jenis_parasit[]" value=""/>
		</span>
	</fieldset>
	<fieldset>
		<span>
			<label>
				Mix, sebutkan.....
			</label>
			<input type="text" name="jenis_parasit[]" value=""/>
		</span>
	</fieldset>
	<br/>
	<fieldset class="fieldsetTop">
		<span>
			<label>
				<b>Pengobatan :</b><br/>
				<i>(ACT : DHP, AAQ, )</i>
			</label>
			<span>
				<label class="declabel2"><input type="checkbox" name="pengobatan[]" value="ACT"> ACT</label>
				<label class="declabel2"><input type="checkbox" name="pengobatan[]" value="PRIMAQUINE"> PRIMAQUINE</label>
				<label class="declabel2"><input type="checkbox" name="pengobatan[]" value="KINA"> KINA</label>
				<label class="declabel2">
					<input type="checkbox" name="pengobatan[]" value="Lainnya" id="lainnya"> Lainnya (sebutkan)...
					<input type="text" name="text" value="" onchange="$('#lainnya').'Lainnya'.val(this.value)"/>
				</label>
				
			</span>
		</span>
	</fieldset>
	<br/>
	<fieldset class="fieldsetTop">
		<span>
			<label>
				<b>Keadaan Malaria :</b><br/>
			</label>
			<span>
				<label class="declabel2"><input type="radio" name="keadaan_malaria[]" value="Malaria tanpa komplikasi"> Malaria tanpa komplikasi</label>
				<label class="declabel2"><input type="radio" name="keadaan_malaria[]" value="Malaria dengan komplikasi"> Malaria dengan komplikasi</label>
			</span>
		</span>
	</fieldset>
	<br/>
	<fieldset class="fieldsetTop">
		<span>
			<label>
				<b>Follow Up Pengobatan :</b><br/>
			</label>
			<span>
			<div class="spanfloat">
				<label class="declabel2 min">Hari 4</label>
				<label class="declabel2 min"><input type="radio" name="follow_up[4][]" value="Pos"> Pos</label>
				<label class="declabel2 min"><input type="radio" name="follow_up[4][]" value="Neg"> Neg</label>
			</div>
			<div class="spanfloat">
				<label class="declabel2 min">Hari 14</label>
				<label class="declabel2 min"><input type="radio" name="follow_up[14][]" value="Pos"> Pos</label>
				<label class="declabel2 min"><input type="radio" name="follow_up[14][]" value="Neg"> Neg</label>
			</div>
			<div class="spanfloat">
				<label class="declabel2 min">Hari 28</label>
				<label class="declabel2 min"><input type="radio" name="follow_up[28][]" value="Pos"> Pos</label>
				<label class="declabel2 min"><input type="radio" name="follow_up[28][]" value="Neg"> Neg</label>
			</div>
			<div class="spanfloat">
				<label class="declabel2 min">3 Bulan</label>
				<label class="declabel2 min"><input type="radio" name="follow_up[90][]" value="Pos"> Pos</label>
				<label class="declabel2 min"><input type="radio" name="follow_up[90][]" value="Neg"> Neg</label>
			</div>
			</span>
		</span>
	</fieldset>
	<br/>
	</td><td width="50%" valign="top">
	<fieldset class="fieldsetTop">
		<span>
			<label>
				<b>Efek Samping Pengobatan :</b><br/>
			</label>
			<span>
				<label class="declabel2"><input type="checkbox" name="efek_samping[]" value="Mual"> Mual</label>
				<label class="declabel2"><input type="checkbox" name="efek_samping[]" value="Lemas"> Lemas</label>
				<label class="declabel2"><input type="checkbox" name="efek_samping[]" value="Pusing"> Pusing</label>
				<label class="declabel2"><input type="checkbox" name="efek_samping[]" value="Muntah"> Muntah</label>
				<label class="declabel2"><input type="checkbox" name="efek_samping[]" value="Pingsang"> Pingsang</label>
				<label class="declabel2"><input type="checkbox" name="efek_samping[]" value="Kejang"> Kejang</label>
				<label class="declabel2"><input type="checkbox" name="efek_samping[]" value="Sakit Kepala"> Sakit Kepala</label>
			</span>
		</span>
	</fieldset>
	<br/>
	<fieldset class="fieldsetTop">
		<span>
			<label>
				<b>Rujukan Penderita :</b>
			</label>
		</span>
	</fieldset>
	<fieldset class="fieldsetTop">
		<span>
			<label>
				A. Dirujuk Dari : 
			</label>
			<span>
				<label class="declabel2"><input type="radio" name="dirujuk_dari[]" value="Pustu"> Pustu</label>
				<label class="declabel2"><input type="radio" name="dirujuk_dari[]" value="Poskesdes"> Poskesdes</label>
				<label class="declabel2"><input type="radio" name="dirujuk_dari[]" value="Polindes/Bidan Desa"> Polindes/Bidan Desa</label>
				<label class="declabel2"><input type="radio" name="dirujuk_dari[]" value="Klinik/Praktek Swasta"> Klinik/Praktek Swasta</label>
				<label class="declabel2"><input type="radio" name="dirujuk_dari[]" value="Kader/Posmaldes"> Kader/Posmaldes</label>
			</span>
		</span>
	</fieldset>
	<fieldset class="fieldsetTop">
		<span>
			<label>
				B. Dirujuk Ke : 
			</label>
			<span>
				<label class="declabel2"><input type="radio" name="dirujuk_ke[]" value="Rumah Sakit"> Rumah Sakit</label>
				<label class="declabel2"><input type="radio" name="dirujuk_ke[]" value="Puskesmas  lain"> Puskesmas  lain</label>
			</span>
		</span>
	</fieldset>
	<br/>
	<fieldset class="fieldsetTop">
		<span>
			<label>
				<b>Hasil Akhir Pengobatan :</b> 
			</label>
			<span>
				<label class="declabel2"><input type="radio" name="hasil_obat[]" value="SEMBUH"> SEMBUH</label>
				<label class="declabel2"><input type="radio" name="hasil_obat[]" value="MENINGGAL"> MENINGGAL</label>
				<label class="declabel2"><input type="radio" name="hasil_obat[]" value="Follow Up Tidak Lengkap"> Follow Up Tidak Lengkap</label>
			</span>
			<label style="width:100%;">
				<i>hasil pengobatan: diisi pada saat follow up terakhir merujuk pertanyaan no.19 </i>
			</label>
		</span>
		<span>
			<label>
				<b>Gagal Pengobatan :</b>
			</label>
			<span>
				<label class="declabel2"><input type="checkbox" name="gagal_obat[]" value="Faktor Obat"> Faktor Obat</label>
				<label class="declabel2"><input type="checkbox" name="gagal_obat[]" value="Kepatuhan"> Kepatuhan</label>
			</span>
		</span>
	</fieldset>
	<br/>
	<fieldset class="fieldsetTop">
		<span>
			<label>
				<b>KLASIFIKASI ASAL PENULARAN</b>
			</label>
			<span>
				<label class="declabel2"><input type="radio" name="asal_penularan[]" value="Indigenous"> Indigenous</label>
				<label class="declabel2"><input type="radio" name="asal_penularan[]" value="Import"> Import</label>
				<label class="declabel2"><input type="radio" name="asal_penularan[]" value="Tidak Diketahui"> Tidak Diketahui</label>
			</span>
			<label style="width:100%;">
				<i>(diisi pada tahap eliminasi dan pemeliharaan)</i>
			</label>
		</span>
	</fieldset>
	<br/>
	<fieldset class="fieldsetTop">
		<span>
			<label>
				<b>ASAL KEGIATAN PENEMUAN PENDERITA :</b> 
			</label>
			<span>
				<label class="declabel2"><input type="radio" name="asal_kegiatan_penemuan[]" value="PCD"> PCD</label>
				<label class="declabel2"><input type="radio" name="asal_kegiatan_penemuan[]" value="ACD"> ACD</label>
				<label class="declabel2"><input type="radio" name="asal_kegiatan_penemuan[]" value="MBS"> MBS</label>
				<label class="declabel2"><input type="radio" name="asal_kegiatan_penemuan[]" value="MFS"> MFS</label>
				<label class="declabel2"><input type="radio" name="asal_kegiatan_penemuan[]" value="Survei Kontak"> Survei Kontak</label>
				<label class="declabel2"><input type="radio" name="asal_kegiatan_penemuan[]" value="Kader"> Kader</label>
				<label class="declabel2"><input type="radio" name="asal_kegiatan_penemuan[]" value="Follow up"> Follow up</label>
				<label class="declabel2"><input type="radio" name="asal_kegiatan_penemuan[]" value="PE"> PE</label>
			</span>
		</span>
	</fieldset>
	</td></tr></table>
	<br />
	<!--
	<fieldset>
		<span>
		<label>Dokumen Pemeriksaan*</label>
		<input type="file" name="filedok" value="" />		
		</span>
	</fieldset>
	<fieldset>
		<span>
		<label>&nbsp;</label>
		<a href="<?=base_url()?>tmp/register_malaria.xls" style="color:blue">Klik di Sini Untuk Mengunduh File Malaria</a>
		</span>
	</fieldset>
	-->
	<fieldset>
		<span>
		<input type="submit" name="bt1" value="Proses Data"/>
		</span>
	</fieldset>	
</form>
</div >