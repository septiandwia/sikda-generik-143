<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require_once 'include/xmlelement.php';
class T_malaria extends CI_Controller {
	public function index()
	{
		$this->load->view('t_malaria/v_t_malaria');
	}
	
	public function t_malariaxml()
	{
		$this->load->model('t_malaria_model');
		
		$limit = $this->input->post('rows')?$this->input->post('rows'):10;
		
		$paramstotal=array(
					'tahun'=>$this->input->post('tahun'),
					'carinama'=>$this->input->post('carinama')
					);
					
		$total = $this->t_malaria_model->totalmalaria($paramstotal);
		
		$total_pages = ($total >0)?ceil($total/$limit):1;
		$page = $this->input->post('page')?$this->input->post('page'):1;
		if ($page > $total_pages) $page=$total_pages;
		$start = $limit*$page - $limit;
		
		$params=array(
					'start'=>$start,
					'limit'=>$limit,
					'sort'=>$this->input->post('asc'),
					'tahun'=>$this->input->post('tahun'),
					'carinama'=>$this->input->post('carinama')
					);
					
		$result = $this->t_malaria_model->getmalaria($params);		
		
		header("Content-type: text/xml");
		echo writeXmlElement::writeXml3('rows', $result['data'], $total,$page,$total_pages);
	}
	
	public function add()
	{
		$this->load->helper('beries_helper');
		$this->load->view('t_malaria/v_t_malaria_add');
	}
	
	public function addprocess()
	{	
		$this->load->library('form_validation');

		$val = $this->form_validation;

		$arrayberkunjung = $this->input->post('pasienberkunjung_final')?json_decode($this->input->post('pasienberkunjung_final',TRUE)):NULL;
		
							
			$db = $this->load->database('sikda', TRUE);
			$db->trans_begin();
			

			if($arrayberkunjung){
                $irow3=1;
                foreach($arrayberkunjung as $rowkunjunganloop){
                    $datakunjungantmp = json_decode($rowkunjunganloop);
                    $datakunjunganloop[] = array(
                        'iROW' => $irow3,
						'DESA' => $datakunjungantmp->master_kelurahan_id,
						'KECAMATAN' => $datakunjungantmp->master_kecamatan_id,
						'KAB_KOTA' => $datakunjungantmp->master_kabupaten_id,
						'TGL_BERKUNJUNG' => date("Y-m-d", strtotime(str_replace('/', '-',$datakunjungantmp->tgl_kunjung)))
                    );
                    $datadiagnosainsert = $datakunjunganloop;
                    $irow3++;
                }
            }


			$regmal = $this->input->post('kd_puskesmas').'-'.$this->input->post('nik', TRUE);
			$dataexc = array(
			'BULAN' => $this->input->post('bulan',true),
			'TAHUN' => $this->input->post('tahun',true),
			'REG_MALARIA' 	=> $regmal,
			'NO_PENGENAL' 	=> $this->input->post('nik', TRUE),
			'NAMA_PENDERITA'=> $this->input->post('nama_penderita', TRUE),
			'KD_PUSKESMAS' => $this->input->post('kd_puskesmas'),
			'PEMERIKSA' 	=> $this->input->post('pemeriksa')?json_encode($this->input->post('pemeriksa',TRUE)):'',
			'GEJALA' 		=> $this->input->post('gejala')?json_encode($this->input->post('gejala',TRUE)):'',
			'TGL_GEJALA' 		=> date("Y-m-d", strtotime(str_replace('/', '-',$this->input->post('tgl_gejala')))),
			'PASIEN_BERKUNJUNG' => json_encode($datadiagnosainsert),
			'TGL_PERIKSA' 	=> date('Y-m-d', strtotime($this->input->post('tgl_periksa'))),
			'METODE_PEMERIKSAAN' => $this->input->post('metode_pemeriksaan')?json_encode($this->input->post('metode_pemeriksaan',TRUE)):'',
			'JENIS_PARASIT' 	=> $this->input->post('jenis_parasit')?json_encode($this->input->post('jenis_parasit',TRUE)):'',
			'PENGOBATAN' 	=> $this->input->post('pengobatan')?json_encode($this->input->post('pengobatan',TRUE)):'',
			'KEADAAN_MALARIA' => $this->input->post('keadaan_malaria')?json_encode($this->input->post('keadaan_malaria',TRUE)):'',
			'FOLLOW_UP_H4' 	=> $this->input->post('follow_up')?json_encode($this->input->post('follow_up',TRUE)):'',
			'EFEK_SAMPING' 	=> $this->input->post('efek_samping')?json_encode($this->input->post('efek_samping',TRUE)):'',
			'DIRUJUK_DARI' 	=> $this->input->post('dirujuk_dari')?json_encode($this->input->post('dirujuk_dari',TRUE)):'',
			'DIRUJUK_KE' 	=> $this->input->post('dirujuk_ke')?json_encode($this->input->post('dirujuk_ke',TRUE)):'',
			'HASIL_OBAT' 	=> $this->input->post('hasil_obat')?json_encode($this->input->post('hasil_obat',TRUE)):'',
			'GAGAL_OBAT' 	=> $this->input->post('gagal_obat')?json_encode($this->input->post('gagal_obat',TRUE)):'',
			'ASAL_PENULARAN' => $this->input->post('asal_penularan')?json_encode($this->input->post('asal_penularan',TRUE)):'',
			'ASAL_KEGIATAN_PENEMUAN'	=> $this->input->post('asal_kegiatan_penemuan')?json_encode($this->input->post('asal_kegiatan_penemuan',TRUE)):'',
			'ninput_tgl' => date('Y-m-d H:i:s')
			
			);
			
			if(isset($_FILES['fileToUpload']) && $_FILES['fileToUpload']['name'] != ""){
			
			//input register malaria
			$data = $this->readExcelMalaria($_FILES['fileToUpload']['tmp_name']);	
			
			}			
			
			$db->insert('kunjungan_malaria', $dataexc);
			
			if ($db->trans_status() === FALSE)
			{
				$db->trans_rollback();
				die('Maaf Proses Insert Data Gagal');
			}
			else
			{
				$db->trans_commit();
				die('OK');
			}
	}
	
	public function editprocess()
	{				
		$this->load->library('form_validation');
		$val = $this->form_validation;
		
		$arrayberkunjung = $this->input->post('pasienberkunjung_final')?json_decode($this->input->post('pasienberkunjung_final',TRUE)):NULL;
							
			$db = $this->load->database('sikda', TRUE);
			$db->trans_begin();
			
			if($arrayberkunjung){
                $irow3=1;
                foreach($arrayberkunjung as $rowkunjunganloop){
                    $datakunjungantmp = json_decode($rowkunjunganloop);
                    $datakunjunganloop[] = array(
                        'iROW' => $irow3,
						'DESA' => $datakunjungantmp->master_kelurahan_id,
						'KECAMATAN' => $datakunjungantmp->master_kecamatan_id,
						'KAB_KOTA' => $datakunjungantmp->master_kabupaten_id,
						'TGL_BERKUNJUNG' => date("Y-m-d", strtotime(str_replace('/', '-',$datakunjungantmp->tgl_kunjung)))
                    );
                    $datadiagnosainsert = $datakunjunganloop;
                    $irow3++;
                }
            }
			
			$dataexc = array(
				'BULAN' => $this->input->post('bulan',true),
				'TAHUN' => $this->input->post('tahun',true),
				'NO_PENGENAL' 	=> $this->input->post('no_pengenal'),
				'NAMA_PENDERITA'=> $this->input->post('NAMA_PENDERITA'),
				'PEMERIKSA' 	=> $this->input->post('pemeriksa')?json_encode($this->input->post('pemeriksa',TRUE)):'',
				'GEJALA' 		=> $this->input->post('gejala')?json_encode($this->input->post('gejala',TRUE)):'',
				'TGL_GEJALA' 		=> date("Y-m-d", strtotime(str_replace('/', '-',$this->input->post('tgl_gejala')))),
				'PASIEN_BERKUNJUNG' => json_encode($datadiagnosainsert),
				'TGL_PERIKSA' 	=> date('Y-m-d', strtotime($this->input->post('tgl_periksa'))),
				'METODE_PEMERIKSAAN' => $this->input->post('metode_pemeriksaan')?json_encode($this->input->post('metode_pemeriksaan',TRUE)):'',
				'JENIS_PARASIT' 	=> $this->input->post('jenis_parasit')?json_encode($this->input->post('jenis_parasit',TRUE)):'',
				'PENGOBATAN' 	=> $this->input->post('pengobatan')?json_encode($this->input->post('pengobatan',TRUE)):'',
				'KEADAAN_MALARIA' => $this->input->post('keadaan_malaria')?json_encode($this->input->post('keadaan_malaria',TRUE)):'',
				'FOLLOW_UP_H4' 	=> $this->input->post('follow_up')?json_encode($this->input->post('follow_up',TRUE)):'',
				'EFEK_SAMPING' 	=> $this->input->post('efek_samping')?json_encode($this->input->post('efek_samping',TRUE)):'',
				'DIRUJUK_DARI' 	=> $this->input->post('dirujuk_dari')?json_encode($this->input->post('dirujuk_dari',TRUE)):'',
				'DIRUJUK_KE' 	=> $this->input->post('dirujuk_ke')?json_encode($this->input->post('dirujuk_ke',TRUE)):'',
				'HASIL_OBAT' 	=> $this->input->post('hasil_obat')?json_encode($this->input->post('hasil_obat',TRUE)):'',
				'GAGAL_OBAT' 	=> $this->input->post('gagal_obat')?json_encode($this->input->post('gagal_obat',TRUE)):'',
				'ASAL_PENULARAN' => $this->input->post('asal_penularan')?json_encode($this->input->post('asal_penularan',TRUE)):'',
				'ASAL_KEGIATAN_PENEMUAN'	=> $this->input->post('asal_kegiatan_penemuan')?json_encode($this->input->post('asal_kegiatan_penemuan',TRUE)):'',
				'nupdate_oleh' => $this->session->userdata('nusername'),
				'nupdate_tgl' => date('Y-m-d H:i:s')
			);
			
			$id=$this->input->post('ID',true);
			
			$db->where('ID',$id);
			$db->update('kunjungan_malaria', $dataexc);
			
			if ($db->trans_status() === FALSE)
			{
				$db->trans_rollback();
				die('Maaf Proses Insert Data Gagal');
			}
			else
			{
				$db->trans_commit();
				die('OK');
			}
					
	}
	
	public function edit()
	{
		$this->load->helper('beries_helper');
		$id=$this->input->get('id')?$this->input->get('id',TRUE):null;
		$db = $this->load->database('sikda', TRUE);
		$val = $db->query("select * 
						from kunjungan_malaria
						where ID = '".$id."'")->row();
		$data['data']=$val;		
		$this->load->view('t_malaria/v_t_malaria_edit',$data);
	}
	
	public function delete()
	{
		$id=$this->input->post('id')?$this->input->post('id',TRUE):null;
		$db = $this->load->database('sikda', TRUE);
		if($db->query("delete from kunjungan_malaria where ID = '".$id."'")){
			die(json_encode('OK'));
		}else{
			die(json_encode('FAIL'));
		}
	}
	
	public function detail()
	{
		$id=$this->input->get('id')?$this->input->get('id',TRUE):null;
		$db = $this->load->database('sikda', TRUE);
		$val = $db->query("select * 
						from kunjungan_malaria
						where ID = '".$id."'")->row();
		$data['data']=$val;
		$this->load->view('t_malaria/v_t_malaria_detail',$data);
	}
	
	public function pasienberkunjung_xml()
	{
		//error_reporting(1);
		//ini_set('display_errors', '1');
		$this->load->model('t_malaria_model');
		
		$limit = $this->input->post('rows')?$this->input->post('rows'):5;
		
		$paramstotal=array(
					'id1'=>$this->input->post('myid1')
					);
					
		$total = $total = $this->t_malaria_model->totalPasienberkunjung($paramstotal);
		
		$total_pages = ($total >0)?ceil($total/$limit):1;
		$page = $this->input->post('page')?$this->input->post('page'):1;
		if ($page > $total_pages) $page=$total_pages;
		$start = $limit*$page - $limit;
			
		$params=array(
					'start'=>$start,
					'limit'=>$limit,
					'sort'=>$this->input->post('asc'),
					'id1'=>$this->input->post('myid1')
					);
					
		$result = $this->t_malaria_model->getPasienberkunjung($params);		
		
		
		header("Content-type: text/xml");
		echo writeXmlElement::writeXml3('rows', $result['data'], $total,$page,$total_pages);
	}
	
	public function pasienberkunjung_arr()
	{
		$id=$this->input->get('id')?$this->input->get('id',TRUE):null;
		$db = $this->load->database('sikda', TRUE);
		$val = $db->query("select PASIEN_BERKUNJUNG from kunjungan_malaria
						where ID = 18")->result_array();
		$data['data']=$val;
		
		$pb = $val[0][PASIEN_BERKUNJUNG];
		
		$result['data']= json_decode($pb);
		
		echo "<pre>";
		print_r($result);
		echo "</pre>";
		
	}
	
	/*
	function readExcelMalaria($file){
		$this->load->library('PHPExcel/Classes/PHPExcel');
		$db = $this->load->database('sikda', TRUE);
		
		$objReader = new PHPExcel_Reader_Excel2007();
		$objReader->setReadDataOnly(true);
		$objPHPExcel = $objReader->load($file);

		$data =  array();
		
		$sheet = 'regmal';
		$worksheet = $objPHPExcel->setActiveSheetIndexByName($sheet);
		foreach ($worksheet->getRowIterator() as $row) {
		  $cellIterator = $row->getCellIterator();
		  $cellIterator->setIterateOnlyExistingCells(false);
		  foreach ($cellIterator as $cell) {
			$data[$sheet][$cell->getRow()][$cell->getColumn()] = $cell->getValue();
		  }
		}
		
		$montharray = bulan();
		
		$rekap = $data['regmal'];
		
					
		$nama_puskesmas = $rekap['5']['E'];
		$kode_puskesmas = $rekap['5']['F'];
		$data_bulan		= $rekap['6']['E'];
		$data_bulan 	= explode(' ',$data_bulan);
		
		$bulan = array();
		$bulan['NAMA_PUSKESMAS'] 	= $nama_puskesmas;
		$bulan['KODE_PUSKESMAS'] 	= $kode_puskesmas;
		$bulan['BULAN'] 			= date('Y-m-01', strtotime($data_bulan[0]."-".$montharray[$data_bulan[1]]));
		
		//-------------start malaria excel--------------------//
		$rank = 1;
		for($i=16; $i<=25; $i++){ 			//array bulan
			$rekap_row = $rekap[$i];			
			
			$array = array();
			$array['ID'] 					= $bulan['BULAN'].$kode_puskesmas."XLS".$rank;
			$array['TYPE'] 					= 'XLS';
			//$array['MONTH'] 				= $bulan['BULAN'];
			$array['KD_PUSKESMAS'] 			= $kode_puskesmas;
			//$array['PUSKESMAS'] 			= $nama_puskesmas;
			$array['REG_MALARIA'] 			= $rekap_row['B'];
			$array['ASAL_KEGIATAN_PENEMUAN']= $rekap_row['C'];
			$array['NAMA_PENDERITA'] 		= $rekap_row['D'];
			$array['UMUR_PENDERITA']		= $rekap_row['E'];
			$array['SATUAN_UMUR'] 			= $rekap_row['F'];
			$array['PEMERIKSA'] 			= $rekap_row['M'];
			$array['GEJALA']				= $rekap_row['N'];
			$array['TGL_GEJALA']	 		= $rekap_row['Q'];
			//$array['PASIEN_BERKUNJUNG']		= $rekap_row['E'];
			//$array['TGL_BERKUNJUNG'] 		= $rekap_row['F'];
			$array['HISTORY_OBAT'] 			= $rekap_row['B'];
			$array['TGL_PERIKSA']			= $rekap_row['C'];
			$array['METODE_PEMERIKSAAN'] 	= $rekap_row['D'];
			$array['JENIS_PARASIT']		= $rekap_row['E'];
			$array['PENGOBATAN'] 			= $rekap_row['F'];
			$array['KEADAAN_MALARIA'] 			= $rekap_row['M'];
			$array['FOLLOW_UP_H4']				= $rekap_row['C'];
			$array['FOLLOW_UP_H14']	 		= $rekap_row['D'];
			$array['FOLLOW_UP_H28']		= $rekap_row['E'];
			$array['FOLLOW_UP_B3'] 		= $rekap_row['F'];
			$array['EFEK_SAMPING']		= $rekap_row['E'];
			$array['DIRUJUK_DARI'] 			= $rekap_row['F'];
			$array['DIRUJUK_KE'] 			= $rekap_row['M'];
			$array['HASIL_OBAT']				= $rekap_row['C'];
			$array['GAGAL_OBAT']	 		= $rekap_row['D'];
			$array['ASAL_PENULARAN']		= $rekap_row['E'];
												
			$query = $db->get_where('register_malaria', array('ID'=>$array['ID']));
			if ($query->num_rows() > 0){
				$db->where('ID', $array['ID']);
				$db->update('register_malaria', $array); 
				$array['ACTION'] = 'Update';
			}else{
				$db->insert('register_malaria', $array); 
				$array['ACTION'] = 'Insert';
			}
			$bulan['ROWS']['XLS'][] = $array;
			
			$rank++;
		}		
		//-------------end   malaria excel--------------------//
		
		
		
		//return $bulan;
	}
	*/
	
}

/* End of file c_ds_datadasar.php */
/* Location: ./application/controllers/c_ds_datadasar.php */