<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$active_group = 'sikda';
$active_record = TRUE;

$db['sikda']['hostname'] = 'localhost';
$db['sikda']['username'] = 'root';
$db['sikda']['password'] = 'root';
$db['sikda']['database'] = 'sikda_puskesmas';
$db['sikda']['dbdriver'] = 'mysqli';
$db['sikda']['dbprefix'] = '';
$db['sikda']['pconnect'] = TRUE;
$db['sikda']['db_debug'] = TRUE;
$db['sikda']['cache_on'] = FALSE;
$db['sikda']['cachedir'] = '';
$db['sikda']['char_set'] = 'utf8';
$db['sikda']['dbcollat'] = 'utf8_general_ci';
$db['sikda']['swap_pre'] = '';
$db['sikda']['autoinit'] = TRUE;
$db['sikda']['stricton'] = FALSE;



/* End of file database.php */
/* Location: ./application/config/database.php */
