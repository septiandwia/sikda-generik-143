<?php
class T_malaria_model extends CI_Model {

  private $db;
  public function __construct()
  {
	parent::__construct();
	$this->db = $this->load->database('sikda', TRUE);
  }
  
	public function getmalaria($params)
	{
		$this->db->select("u.ID, u.no_pengenal AS nik, u.nama_penderita AS nama, p.PUSKESMAS, MONTHNAME(STR_TO_DATE(u.BULAN,'%m')) as BULAN, u.TAHUN, u.ID as nid", false );
		$this->db->from('kunjungan_malaria u');
		//$this->db->join('mst_kelurahan k','k.KD_KELURAHAN=u.KD_KELURAHAN');
		//$this->db->join('mst_kecamatan kc','kc.KD_KECAMATAN=u.KD_KECAMATAN');
		$this->db->join('mst_puskesmas p','p.KD_PUSKESMAS=u.KD_PUSKESMAS');
		//$this->db->where('kc.KD_KABUPATEN', $this->session->userdata('kd_kabupaten'));
		//if($params['tahun']){	$this->db->where('u.TAHUN =', $params['tahun']);}
		if($params['carinama']){
			$this->db->where('u.NAMA_PENDERITA like', '%'.$params['carinama'].'%');
		}
		if($this->session->userdata('group_id')=='all'){
			$this->db->where('u.KD_PUSKESMAS', $this->session->userdata('kd_puskesmas'));
		}
		$this->db->limit($params['limit'],$params['start']);
		$this->db->order_by('u.TAHUN '.$params['sort']);
		
		$result['data'] = $this->db->get()->result_array();//die($this->db->last_query());
		return $result;
	}
    
	public function totalmalaria($params)
	{
		$this->db->select("count(*) as total");
		$this->db->from('kunjungan_malaria');
		
		if($params['carinama']){
			$this->db->where('NAMA_PENDERITA like', '%'.$params['carinama'].'%');
		}
		if($this->session->userdata('group_id')=='all'){
			$this->db->where('KD_PUSKESMAS', $this->session->userdata('kd_puskesmas'));
		}
		$total = $this->db->get()->row();
		return $total->total;
	}

	public function getPasienberkunjung($params)
	{
		$this->db->select("PASIEN_BERKUNJUNG",false);
		$this->db->from('kunjungan_malaria');
		$this->db->where('REG_MALARIA',$params['id1']);
		//$this->db->where('KD_PUSKESMAS',$params['idpuskesmas']);
		$this->db->limit(1);
		$arrpb = $this->db->get()->result_array();

		$jdc = $arrpb[0][PASIEN_BERKUNJUNG];
		$result['data'] = json_decode($jdc);
		
		return $result;
		
	}
	
	public function totalPasienberkunjung($params)
	{
		$this->db->select("count(*) as total");
		$this->db->from('kunjungan_malaria');
		$this->db->where('REG_MALARIA',$params['id1']);
		$this->db->where('KD_PUSKESMAS',$params['idpuskesmas']);
		
		$total = $this->db->get()->row();
		return $total->total;
	}
 
}
